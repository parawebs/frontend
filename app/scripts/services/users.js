'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.users
 * @description
 * # users
 * Factory in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
 .factory('users', function(config, $resource) {
        return $resource(config.getData('url') + 'api/usuarios', {
            id: '@_id'
        }, {
            check: {
                method: 'POST'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    });
