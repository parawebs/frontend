'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.buyticket
 * @description
 * # buyticket
 * Factory in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
  .factory('buyticket', function(config, $resource) {
        return $resource(config.getData('url') + 'buy/ticket', {
            id: '@_id'
        }, {
            create: {
                method: 'POST'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    })
