'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.config
 * @description
 * # config
 * Service in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
  .factory('config', function() {
        // Service logic
        var data = [];
        data.tickets  = 
[{"id":"1","name":"GENERAL","price":"1500","quantity":"-1","key":"q"},{"id":"2","name":"MEDIOS","price":"2500","quantity":"-1","key":"e"},{"id":"3","name":"VIP","price":"4000","quantity":"-1","key":"w"},{"id":"4","name":"PLATINIUM","price":"6250","quantity":"-1","key":"r"}];
        data.url = "http://192.168.1.101/rest/"
        return {
            setData: function(key, d) {
                data[key] = d;
            },
            getData: function(key) {
                return data[key];
            }
        };
    });