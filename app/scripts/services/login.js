'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.login
 * @description
 * # login
 * Service in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
  .factory('login', function(config,$resource) {
  return $resource(config.getData('url') +'index.php/login', { id: '@_id' }, {
    check: {
      method: 'POST'
    }
  });
});