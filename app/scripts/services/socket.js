'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.socket
 * @description
 * # socket
 * Service in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
 .factory('socket', ['$rootScope', function($rootScope) {
        var socket = io.connect('ws://192.168.1.101:3000');
        console.log("socket created");

        return {
            on: function(eventName, callback) {
                function wrapper() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        callback.apply(socket, args);
                    });
                }

                socket.on(eventName, wrapper);

                return function() {
                    socket.removeListener(eventName, wrapper);
                };
            },

            emit: function(eventName, data, callback) {
                socket.emit(eventName, data, function() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                });
            }
        };
    }]);
