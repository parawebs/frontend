'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.report
 * @description
 * # report
 * Factory in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
    .factory('report', function(config, $resource) {
        return $resource(config.getData('url') + 'index.php/report/33/:user/:date', {
            user: '@user',
            date: '@date'
        }, {
            check: {
                method: 'POST'
            },
            query: {
                method: 'GET',
                isArray: true
            }
        });
    });
