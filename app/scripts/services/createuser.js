'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.createUser
 * @description
 * # createUser
 * Factory in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
   .factory('createUser', function(config, $resource) {
        return $resource(config.getData('url') + 'api/usuarios', {
            id: '@_id'
        }, {
            check: {
                method: 'POST'
            },
            query: {
                method: 'GET',
                isArray: true
            }
        });
    });
