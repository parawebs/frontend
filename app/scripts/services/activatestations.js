'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.activateStations
 * @description
 * # activateStations
 * Factory in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
    .factory('activateStations', function(config, $resource) {
        return $resource(config.getData('url') + 'activate/workstation', {
            id: '@_id'
        }, {
            create: {
                method: 'POST'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    })
