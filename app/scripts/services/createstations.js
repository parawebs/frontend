'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.createStations
 * @description
 * # createStations
 * Factory in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
  .factory('Stations', function(config, $resource) {
        return $resource(config.getData('url') + 'api/taquillas', {
            id: '@_id'
        }, {
            create: {
                method: 'POST'
            },
            query: {
                method: 'GET',
                isArray: false
            }
        });
    })