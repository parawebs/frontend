'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.themeconfig
 * @description
 * # themeconfig
 * Service in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
 .config(function ($mdThemingProvider) {    

    var customBackground = {
    '50': '#fafafa',
    '100': '#f5f5f5',
    '200': '#eeeeee',
    '300': '#e0e0e0',
    '400': '#bdbdbd',
    '500': '#000000',
    '600': '#757575',
    '700': '#616161',
    '800': '#424242',
    '900': '#212121',
    '1000': '#000000',
    'A100': '#ffffff',
    'A200': '#eeeeee',
    'A400': '#bdbdbd',
    'A700': '#616161',
    'contrastDefaultColor': 'light',
    'contrastLightColors': '500 600 700 800 900'
  }
    $mdThemingProvider
        .definePalette('customBackground', 
                        customBackground);

   $mdThemingProvider.theme('default')
       .primaryPalette('customBackground')
       .accentPalette('blue');
});