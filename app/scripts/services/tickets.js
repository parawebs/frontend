'use strict';

/**
 * @ngdoc service
 * @name venticketsPosApp.tickets
 * @description
 * # tickets
 * Service in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
    .factory('tickets', function(config, $resource) {
        return $resource(config.getData('url') + 'ticket', {
            id: '@_id'
        }, {
            check: {
                method: 'POST'
            },
            query: {
                method: 'GET',
                isArray: true
            }
        });
    });
