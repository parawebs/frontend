'use strict';

/**
 * @ngdoc function
 * @name venticketsPosApp.controller:ReportCtrl
 * @description
 * # ReportCtrl
 * Controller of the venticketsPosApp
 */
angular.module('venticketsPosApp')
    .controller('ReportCtrl', function($scope, users, report, $timeout, $q) {
        users.query({}, function(r) {
			$scope.users = r.items.values
        });
        $scope.dataReport = [];
        $scope.report = [];
        $scope.getReport = function() {
            var deferred = $q.defer();
            $scope.promise = deferred.promise;
            report.query({
                user: typeof($scope.report.user) === 'undefined' ? "all" : $scope.report.user,
                date: typeof($scope.report.date) === 'undefined' ? "2015-01-08" : moment($scope.report.date).format('YYYY-MM-DD')

            }, function(r) {
                deferred.resolve();
                $scope.dataReport = r;
                $scope.total = []
                $scope.total.base = 0
                $scope.total.impuesto = 0
                $scope.total.cantidad = 0
                $scope.total.iva = 0
                $scope.total.total = 0
                angular.forEach(r, function(v, k) {

                    $scope.total.base = $scope.total.base + parseFloat(v.vol_volbase);
                    $scope.total.impuesto = $scope.total.impuesto + parseFloat(v.vol_volimp);
                    $scope.total.cantidad = $scope.total.cantidad + parseInt(v.vol_can);
                    $scope.total.iva = $scope.total.iva + parseInt(v.vol_voiva);
                    $scope.total.total = $scope.total.total + parseFloat(v.vol_total);
                });
            });


        };



    });
// code
