'use strict';

/**
 * @ngdoc function
 * @name venticketsPosApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the venticketsPosApp
 */
angular.module('venticketsPosApp')
    .controller('UsersCtrl', function($scope, lodash, users, createUser) {
        users.query({}, function(r) {
            $scope.users = r.items.values
         
        });

        $scope.master = {};
        $scope.clearValue = function() {
            $scope.user = angular.copy($scope.master);
            $scope.myForm.$setPristine();
        };

        $scope.save = function() {
            createUser.save($scope.user);
            $scope.users.push($scope.user);
            $scope.user = undefined;
            $scope.myForm.$setPristine();
            $scope.myForm.$setUntouched();
        };

        $scope.edit = function(id) {
            var result = lodash.find($scope.users, {
                "name": id
            });

            var s = lodash.findIndex($scope.users, result);

            $scope.user = $scope.users[s];


        };
    });
