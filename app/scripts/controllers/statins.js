'use strict';

/**
 * @ngdoc function
 * @name venticketsPosApp.controller:StatinsCtrl
 * @description
 * # StatinsCtrl
 * Controller of the venticketsPosApp
 */
angular.module('venticketsPosApp')
    .controller('StatinsCtrl', function($scope, Stations, lodash) {       
         Stations.query({}, function(r) {
            $scope.stations = r.items.values
        });
        $scope.master = {};

        $scope.clearValue = function() {
            $scope.station = angular.copy($scope.master);
            $scope.myForm.$setPristine();
        };

        $scope.save = function() {
            Stations.create($scope.station);
            $scope.stations.push($scope.station);
            $scope.station = undefined;
            $scope.myForm.$setPristine();
            $scope.myForm.$setUntouched();

        };

        $scope.edit = function(id) {
            var result = lodash.find($scope.stations, {
                "name": id
            });
            var s = lodash.findIndex($scope.stations, result);
            $scope.station = $scope.stations[s];


        };
    });
