'use strict';

/**
 * @ngdoc function
 * @name venticketsPosApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the venticketsPosApp
 */
angular.module('venticketsPosApp')
    .controller('MainCtrl', function($scope, $timeout, $location, tickets, buyticket, hotkeys, config, lodash, socket) {
        $scope.config = [];
        $scope.config.auth = true;
        $scope.config.tickets = config.getData('tickets');
        $scope.ticket = [];
        $scope.ticket.default = _.minBy($scope.config.tickets, function(o) {
            return o;
        });
        socket.on("stock update", function(data) {
            config.setData('tickets');
            $scope.config.tickets = data;
            $scope.current = $scope.config.tickets[$scope.getCurrent($scope.current.key)]


        });

        $scope.msgshow = false;
        $scope.msg = "OK";
        $scope.wasFilled = false;
        $scope.nroticket = null;
        $scope.montopago = null;
        $scope.current = {
            "key": "default",
            "name": "SELECCIONE EL TIPO DE TICKET"
        };

        hotkeys.add({
            combo: 's',
            description: "print tickets",
            callback: function(e, k) {
                $scope.sellAction();
            }
        });
        hotkeys.add({
            combo: 'd',
            description: "print tickets",
            allowIn: ['INPUT'],
            callback: function(e, k) {
                $scope.$broadcast('sellAction', "Operacion Cancelada");
                $scope.msg = "OK";
                $scope.nroticket = null;
                $scope.montopago = null;
                $scope.current = {
                    "key": "default",
                    "name": "SELECCIONE EL TIPO DE TICKET"
                }
            }
        });


        $scope.getCurrent = function(key) {
            angular.element(document.querySelector('#f_1'))[0].focus();
            return lodash.findKey($scope.config.tickets, {
                'key': key
            });

        };
        $scope.sellAction = function() {
            if ($scope.wasFilled && ($scope.current.key != "default")) {

                if (!$scope.config.auth) {

                    $scope.msg = "No estas Logueado";
                    $timeout(function() {
                        $scope.msgshow = false;
                        $scope.msg = "OK";
                        $location.path("/");

                    }, 2000);
                } else {
                    if (($scope.current.quantity > $scope.nroticket) || ($scope.current.quantity == -1)) {

                        var s = $scope.nroticket + ($scope.nroticket == 1 ? " Ticket " : " Tickets ") + $scope.current.name.toUpperCase();


                        buyticket.save({
                            'vol_tip': $scope.current.id,
                            'vol_can': $scope.nroticket,
                            'act_id': config.getData('auth')
                        }, function(r) {
                            if (!r.error) {
                                $scope.msg = "Imprimiendo";
                                $scope.$broadcast('sellAction', s);
                                $scope.current = $scope.ticket.default;
                                $scope.nroticket = null;
                                $scope.montopago = null;
                                $scope.wasFilled = false;
                                angular.element(
                                    document.querySelector('#f_1'))[0].focus();
                                $timeout(function() {
                                    $scope.msg = "OK";

                                }, 1000);
                            } else {
                                $scope.msg = r.msj;
                            }
                        });

                    } else {
                        $scope.msg = "No hay suficientes tickets disponibles";

                    }
                }
            }
        }
        $scope.$on('sellAction', function(event, mass) {
            $scope.lastUpdate = mass;
        });

        $scope.keypressHandler = function(event, nextIdx) {
            if (event.keyCode == 13) {
                try {
                    angular.element(
                        document.querySelector('#f_' + nextIdx))[0].focus();
                } catch (err) {
                    if (($scope.nroticket != null) && ($scope.nroticket != "")) {
                        angular.element(
                            document.querySelector('#f_2'))[0].blur();
                        if ($scope.current.key == "default") {
                            $scope.msg = "Seleccione un tipo de ticket";
                        } else {
                            $scope.msg = "Presione S para imprimir el ticket.";
                            $scope.wasFilled = true;
                        }


                    }
                }
            }
        }
        angular.forEach($scope.config.tickets, function(value, k) {
            hotkeys.add({
                combo: value.key,
                description: value.name,
                allowIn: ['INPUT'],
                callback: function(e, k) {
                    $scope.current = $scope.config.tickets[$scope.getCurrent(k.combo[0])]

                }
            });
            hotkeys.add({
                combo: 'shift+' + value.key,
                description: value.name,
                allowIn: ['INPUT'],
                callback: function(e, k) {

                    var lastChar = k.combo[0].substr(k.combo[0].length - 1);
                    $scope.ticket.default = $scope.config.tickets[$scope.getCurrent(lastChar)]

                }
            });
        });
        hotkeys.add({
            combo: 'shift+a',
            description: "print tickets",
            callback: function(e, k) {
                $location.path("admin");
            }
        });
    });
