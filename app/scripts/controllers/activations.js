'use strict';

/**
 * @ngdoc function
 * @name venticketsPosApp.controller:ActivationsCtrl
 * @description
 * # ActivationsCtrl
 * Controller of the venticketsPosApp
 */
angular.module('venticketsPosApp')
    .controller('ActivationsCtrl', function($scope, users, Stations,activateStations) {
        users.query({}, function(r) {
            $scope.users = r.items.values
        });
        Stations.query({}, function(r) {
            $scope.stations = r.items.values
        });
        $scope.acts = [];
        $scope.save = function() {
            activateStations.save($scope.act);
            $scope.acts.push($scope.act);
            $scope.act = undefined;
            $scope.myForm.$setPristine();
            $scope.myForm.$setUntouched();
        };

    });
