'use strict';

/**
 * @ngdoc function
 * @name venticketsPosApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the venticketsPosApp
 */
angular.module('venticketsPosApp')
  .controller('AdminCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
