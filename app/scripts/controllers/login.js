'use strict';

/**
 * @ngdoc function
 * @name venticketsPosApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the venticketsPosApp
 */
angular.module('venticketsPosApp')
    .controller('LoginCtrl', function(config, tickets, $scope, $mdToast,$rootScope, $document, login, $location) {
        $scope.user = [];
        $scope.showSimpleToast = function() {

            login.check({
                login: $scope.user.login,
                password: $scope.user.password
            }, function(r) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(r.msj)
                    .position('bottom right')
                    .hideDelay(3000)
                );
                if (!r.error) {
                    $rootScope.loggedUser  = true;
                    $location.path("pos");
                    config.setData("auth", r.act_id);
                    tickets.query(function(data) {
                        config.setData("tickets", data);
                    });
                }
            }); //saves an entry. Assuming $scope.entry is the Entry object  

        };
    }).controller('ToastCtrl', function($scope, $mdToast) {
        $scope.closeToast = function() {
            $mdToast.hide();
        };
    });
