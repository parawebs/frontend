'use strict';

/**
 * @ngdoc overview
 * @name venticketsPosApp
 * @description
 * # venticketsPosApp
 *
 * Main module of the application.
 */
angular
    .module('venticketsPosApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngMessages',
        'ngRoute',
        'ngSanitize',
        'ngMaterial',
        'cfp.hotkeys',
        'ngLodash',
        'ngMdIcons',
        'md.data.table',
        'mdPickers'

    ])
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'login'
            })
            .when('/admin', {
              templateUrl: 'views/admin.html',
              controller: 'AdminCtrl',
              controllerAs: 'admin'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
