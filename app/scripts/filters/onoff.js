'use strict';

/**
 * @ngdoc filter
 * @name venticketsPosApp.filter:onoff
 * @function
 * @description
 * # onoff
 * Filter in the venticketsPosApp.
 */
angular.module('venticketsPosApp')
    .filter('onoff', function() {
        return function(input) {
            return input == 0 ? "Inactivo" : "Activo";
        };
    }).filter('admvend', function() {
        return function(input) {
            return input == 1 ? "Vendedor" : "Administrador";
        };
    }).filter('fixed', function () { //change module name as filters
    return function(input) {
      return parseFloat(input).toFixed(2);
    };
  }); 
