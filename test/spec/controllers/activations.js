'use strict';

describe('Controller: ActivationsCtrl', function () {

  // load the controller's module
  beforeEach(module('venticketsPosApp'));

  var ActivationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActivationsCtrl = $controller('ActivationsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ActivationsCtrl.awesomeThings.length).toBe(3);
  });
});
