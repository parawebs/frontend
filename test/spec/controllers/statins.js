'use strict';

describe('Controller: StatinsCtrl', function () {

  // load the controller's module
  beforeEach(module('venticketsPosApp'));

  var StatinsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StatinsCtrl = $controller('StatinsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(StatinsCtrl.awesomeThings.length).toBe(3);
  });
});
