'use strict';

describe('Service: createStations', function () {

  // load the service's module
  beforeEach(module('venticketsPosApp'));

  // instantiate service
  var createStations;
  beforeEach(inject(function (_createStations_) {
    createStations = _createStations_;
  }));

  it('should do something', function () {
    expect(!!createStations).toBe(true);
  });

});
