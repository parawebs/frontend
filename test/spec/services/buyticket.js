'use strict';

describe('Service: buyticket', function () {

  // load the service's module
  beforeEach(module('venticketsPosApp'));

  // instantiate service
  var buyticket;
  beforeEach(inject(function (_buyticket_) {
    buyticket = _buyticket_;
  }));

  it('should do something', function () {
    expect(!!buyticket).toBe(true);
  });

});
