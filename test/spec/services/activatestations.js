'use strict';

describe('Service: activateStations', function () {

  // load the service's module
  beforeEach(module('venticketsPosApp'));

  // instantiate service
  var activateStations;
  beforeEach(inject(function (_activateStations_) {
    activateStations = _activateStations_;
  }));

  it('should do something', function () {
    expect(!!activateStations).toBe(true);
  });

});
