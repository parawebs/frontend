'use strict';

describe('Service: themeconfig', function () {

  // load the service's module
  beforeEach(module('venticketsPosApp'));

  // instantiate service
  var themeconfig;
  beforeEach(inject(function (_themeconfig_) {
    themeconfig = _themeconfig_;
  }));

  it('should do something', function () {
    expect(!!themeconfig).toBe(true);
  });

});
