'use strict';

describe('Filter: onoff', function () {

  // load the filter's module
  beforeEach(module('venticketsPosApp'));

  // initialize a new instance of the filter before each test
  var onoff;
  beforeEach(inject(function ($filter) {
    onoff = $filter('onoff');
  }));

  it('should return the input prefixed with "onoff filter:"', function () {
    var text = 'angularjs';
    expect(onoff(text)).toBe('onoff filter: ' + text);
  });

});
